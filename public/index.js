
let currentSlide = 0;
const slides = [
    {
        type: 'video',
        current: 0,
        videos: [
            '/izhng2019.mp4',
            '/izh2019.mp4',
            '/makeup20.mp4'     
        ]
    },
    {
        type: 'stats',
        url: 'https://panel.flirtstudio.net:7717/overview?report=stats'
    },
    {
        type: 'youtube',
        current: 0,
        list: [
            'm9k7WgIPK14',
            '8tKKNV5sXUs',
            'qqflFMhkqHM'
        ]
    }
];

setTimeout(playSlide);

function playSlide() {
    const slide = slides[currentSlide];
    currentSlide = (currentSlide + 1) % slides.length;
    switch(slide.type) {
        case 'video':
            slide.current = (slide.current + 1) % slide.videos.length;
            document.getElementById('content').innerHTML = `
            <video autoplay onended="playSlide()">
                <source src="${slide.videos[slide.current]}" type="video/mp4">
            </video>
            `;
            document.getElementsByTagName('video')[0].onclick = function() {this.play()};
            break;
        case 'stats':
            document.getElementById('content').innerHTML = `
                <iframe width="100%" height="100%" src="${slide.url}" style="border:0"></iframe>
            `;
            setTimeout(playSlide, 10000);
            break;
        case 'youtube':
            slide.current = (slide.current + 1) % slide.list.length;
            document.getElementById('content').innerHTML = `<div id="player"></div>`;
            player = new YT.Player('player', {
                height: '100%',
                width: '100%',
                videoId: slide.list[slide.current],
                events: {
                    'onReady': function (event) {
                        event.target.playVideo();
                    },
                    'onStateChange': function (event) {
                        if (event.data == !YT.PlayerState.PLAYING) {
                            playSlide();
                        }
                    }
                }
            });     
            document.getElementsByTagName('iframe')[0].style.visibility = 'visible';
    }
}
